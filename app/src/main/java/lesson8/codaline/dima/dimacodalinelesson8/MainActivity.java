package lesson8.codaline.dima.dimacodalinelesson8;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnStart;
    private Button btnStop;
    private TestReceiver testReceiver = new TestReceiver();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStart = (Button) this.findViewById(R.id.start);
        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    IntentFilter intentFilter = new IntentFilter();
                    intentFilter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);
                    registerReceiver(testReceiver, intentFilter);
                    Toast.makeText(v.getContext(),getString(R.string.start_service),Toast.LENGTH_SHORT).show();
                }catch (Exception er){
                    Toast.makeText(v.getContext(),getString(R.string.error),Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnStop = (Button) this.findViewById(R.id.stop);
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    unregisterReceiver(testReceiver);
                    Toast.makeText(v.getContext(), getString(R.string.stop_service), Toast.LENGTH_SHORT).show();
                }catch (Exception er){
                    Toast.makeText(v.getContext(),getString(R.string.error),Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

    }
}
